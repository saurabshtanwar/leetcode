# new_code
import discord
import pandas as pd
import schedule
import time
from datetime import datetime

pd.options.display.max_colwidth = 10000

intents = discord.Intents.all()
client = discord.Client(command_prefix='!', intents=intents)
leetcode_questions = pd.read_csv(
    r'/Users/saurabhtanwar/Documents/discord_bot/leetcode_questions.csv', dtype=str)
leet_easy = leetcode_questions[leetcode_questions['Difficulty Level'] == "Easy"]
leet_medium = leetcode_questions[leetcode_questions['Difficulty Level'] == "Medium"]
leet_hard = leetcode_questions[leetcode_questions['Difficulty Level'] == "Hard"]

a = leet_easy.sample()
channel_id = 1052763352897699918
ds = client.get_all_channels()
print(a['Question Text'])


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('hi'):
        await message.channel.send('Hello!')

    if message.content.startswith('!problem_easy'):
        random_easy = leet_easy.sample()
        a = '<< Question---> '+random_easy['Question Slug'] + \
            ' >> ' + random_easy['Question Text'] + '  ' + \
            'https://leetcode.com/problems/'+random_easy['Question Slug']
        b = str(a).replace('dtype: object', '')
        if 'NaN' in b:
            random_easy = leet_easy.sample()
            a = '<< '+random_easy['Question Slug'] + \
                ' >> ' + random_easy['Question Text'] + ' '
            b = str(a).replace('dtype: object', '')
        c = b.split(None, 1)[1]
        print(c)
        await message.channel.send(c)

    if message.content.startswith('!problem_hard'):
        random_hard = leet_hard.sample()
        a = '<< Question---> '+random_hard['Question Slug'] + \
            ' >> ' + random_hard['Question Text'] + '  ' + \
            'https://leetcode.com/problems/'+random_hard['Question Slug']
        b = str(a).replace('dtype: object', '')
        if 'NaN' in b:
            random_hard = leet_hard.sample()
            a = '<< '+random_hard['Question Slug'] + \
                ' >> ' + random_hard['Question Text'] + ' '
            b = str(a).replace('dtype: object', '')
        c = b.split(None, 1)[1]
        print(c)
        await message.channel.send(c)

    if message.content.startswith('!problem_medium'):
        random_medium = leet_medium.sample()
        a = '<< Question---> '+random_medium['Question Slug'] + \
            ' >> ' + random_medium['Question Text'] + '  ' + \
            'https://leetcode.com/problems/'+random_medium['Question Slug']
        b = str(a).replace('dtype: object', '')
        if 'NaN' in b:
            random_medium = leet_medium.sample()
            a = '<< '+random_medium['Question Slug'] + \
                ' >> ' + random_medium['Question Text'] + ' '
            b = str(a).replace('dtype: object', '')
        c = b.split(None, 1)[1]
        print(c)
        await message.channel.send(c)

client.run(
    'MTA1NTg4MjEzODEzNTgzMDU4OQ.GaDJXh.c-v2pN46i747LMDC9ZpGBz6Jz2Ym99hXYARqKc')
